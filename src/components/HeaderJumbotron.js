import React from 'react';
import Logo from  '../assets/y-logo-white.png';

export default class HeaderJumbotron extends React.Component {

	render() {
		return (
			<div className="header">
				<div className="header__logo">
					<img src={Logo} alt="Y Logo White"/>
				</div>
				<div className="header__jumbotron container">
					<span>Hello! I'm Rinaldy Pratama</span>
					<h1>Consult, Design, and Develop Websites</h1>
					<p>Have something great in mind? Feel free to contact me.</p>
					<p>I'll help you to make it happen.</p>
					<a href="https://rinaldypratama.com" target="_blank" rel="noopener noreferrer">
						<div className="btn-contact">Let's Make Contact</div>
					</a>
				</div>
			</div>
		);
	}
}
