import React from 'react';

export default class Card extends React.Component {

	render() {
		const {title, index, content } = this.props;
		const icon = index === 0? 'far fa-comments fa-2x':
										index === 1? 'fas fa-paint-brush fa-2x':
										index === 2? 'fas fa-cubes fa-2x':
										index === 3? 'fas fa-bullhorn fa-2x':
										index === 4? 'fas fa-tasks fa-2x':
										'fas fa-chart-line fa-2x'
		return (
			<div className="card-container">
				<div className="card-header">
					<div className="card-header__title">
						{title}			
					</div>
					<div className="card-header__icon">
						<i className={icon}></i>
					</div>
				</div>
				<div className="card-content">
					{content}
				</div>
			</div>
		);
	}
}
