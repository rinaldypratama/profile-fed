import React from 'react';

export default class Footer extends React.Component {

	render() {
		return (
			<div className="footer">
				© 2019 Rinaldy Pratama. All rights reserved.​
			</div>
		);
	}
}
