import React from 'react';
import { CSSTransitionGroup } from 'react-transition-group'

export default class NewsletterPanel extends React.Component {

	render() {
		const { isShow, handleClick } = this.props;
		return (
			<CSSTransitionGroup
      transitionName='news-transition'
      transitionEnterTimeout={800}
      transitionLeaveTimeout={300}>
      {
      	isShow &&
				<div className="sticky-bottom">
					<div className="news">
						<div className="news__title">
							<h1>Get latest updates in web technologies</h1>
						</div>
						<div className="news__content">
							<p>I write articles related to web technologies, such as design trends, development tools, UI/UX case studies and reviews, and more. Sign up to my newsletter to get them all.
							</p>
						</div>
						<div className="news__footer">
							<input type="text" placeholder="Email address"/>
							<div className="news__footer-btn" onClick={handleClick}>Count me in!</div>
						</div>
						<div className="news__close-btn" onClick={handleClick}>
							<i className="fas fa-times fa-xs"></i>
						</div>
					</div>
				</div>
      }
			</CSSTransitionGroup>
		);
	}
}
