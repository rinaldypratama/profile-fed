import React from 'react';
import { CSSTransitionGroup } from 'react-transition-group'

class NotificationPanel extends React.Component {

	constructor(props) {
		super(props);

		this.state = {
			isShow:true
		};
		this.handleShow = this.handleShow.bind(this);
	}

	handleShow(){
		this.setState({
			isShow: !this.state.isShow
		})
	}

	render() {
		const { isShow } = this.state;
		return (
			<CSSTransitionGroup
      transitionName='top-transition'
      transitionEnterTimeout={700}
      transitionLeaveTimeout={700}>
      {
				isShow &&
				<div className="notification">
					<div className="notification__container container">
						<div className="notification__text">
							By accessing and using this website, you acknowledge that you have read and understand our <a href="https://giphy.com/explore/who-wants-cookies" target="_blank" rel="noopener noreferrer">Cookie Policy</a>, <a href="" target="_blank" rel="noopener noreferrer">Privacy Policy</a>, and our <a href="" target="_blank" rel="noopener noreferrer">Terms of Service</a>.
						</div>
						<div className="notification__btn" onClick={this.handleShow}><p>Got it</p></div>
					</div>
				</div>
      }
			</CSSTransitionGroup>
		);
	}
}

export default NotificationPanel;

