import React from 'react';
import Card from './Card';
import {dataHigh} from '../utils/DataHighlights';

export default class Highlights extends React.Component {

	render() {
		return (
			<div className="container highlights">
				<div className="highlights__title">
					<h1>How Can I Help You?</h1>
					<p>Our work then targeted, best practices outcomes social innovation synergy.
					Venture philanthropy, revolutionary inclusive policymaker relief. User-centered
					program areas scale.</p>
				</div>
				<div className="highlights__content">
					{
						dataHigh.map((dt, index)=> <Card title={dt.title} index={index} content={dt.content} key={index} />)
					}
				</div>
			</div>
		);
	}
}
