import React, { Component } from 'react';
import './style/index.scss';
import Notif from './components/NotificationPanel'
import Header from './components/HeaderJumbotron'
import Highlights from './components/Highlights'
import Footer from './components/Footer'
import News from './components/NewsletterPanel'


class App extends Component {

  constructor(props) {
    super(props);

    this.state = {
      newsletterShow:false,
      hold: true
    };
    this.closeNews = this.closeNews.bind(this);
  }

  componentDidMount() {
    window.scrollTo(0, 0)
    window.addEventListener('scroll', this.onScroll, false);
    if(localStorage.timestamp && localStorage.isFirst){
      const localTime = localStorage.getItem('timestamp');
      const now = new Date().getTime();
      const isFirst = localStorage.getItem('isFirst');
      if(((now-localTime) > 600000) || isFirst == 'true'){
        this.setState({
          hold: false
        })
      }
    }else{
      const timeNow = new Date().getTime();
      localStorage.setItem('timestamp', timeNow)
      localStorage.setItem('isFirst', true)
      this.setState({
          hold: false
      })
    }

  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.onScroll, false);
  }

  onScroll = () => {
    if (
      ((window.innerHeight + window.scrollY) >= (document.body.offsetHeight / 3) && !this.state.hold)) {
      this.setState({
        newsletterShow: true
      })
    }
  }

  closeNews(){
    this.setState({
      newsletterShow: false,
      hold: true
    })
    const timeNow = new Date().getTime();
    localStorage.setItem('timestamp', timeNow)
    localStorage.setItem('isFirst', false)
  }

  render() {
    const { newsletterShow } = this.state;
    return (
      <div className="App">
        <Notif/>
        <Header/>
        <Highlights/>
        <Footer/>
        <News isShow={newsletterShow} handleClick={this.closeNews}/>
      </div>
    );
  }
}

export default App;




